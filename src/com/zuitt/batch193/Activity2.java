package com.zuitt.batch193.Activity2;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Activity2 {
    public static void main(String[] args) {

        int year = 1992;
        boolean leap = false;


        if (year % 4 == 0) {


            if (year % 100 == 0) {

                if (year % 400 == 0)
                    leap = true;
                else
                    leap = false;
            }


            else
                leap = true;
        }

        else
            leap = false;

        if (leap)
            System.out.println(year + " is a leap year.");
        else
            System.out.println(year + " is not a leap year.");

        int[] intArray = new int[5];

        intArray[0] = 2;
        intArray[1] = 3;
        intArray[2] = 5;
        intArray[3] = 7;
        intArray[4] = 11;
        
        System.out.println("The first prime number is:" + intArray[0]);

        String stringArray[] = new String[4];

        stringArray[0] = "Red Hair Shanks";
        stringArray[1] = "Monkey D. Luffy";
        stringArray[2] = "Buggy the Clown";
        stringArray[3] = "Marshall D. Teach";

        System.out.println("My friends are:" + Arrays.toString(stringArray));

        HashMap<String, Double> bounties = new HashMap<>();

        bounties.put ("Red Hair Shanks", 4.048);
        bounties.put ("Monkey D. Luffy", 3.000);
        bounties.put ("Buggy the Clown", 1.500);
        bounties.put ("Marshall D. Teach", 4.000);

        System.out.println("The current bounties of the Yonkos of the New World: " + bounties);











    }
}
